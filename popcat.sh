chmod +x popularwords/filter.py
python2 popularwords/filter.py popularwords/popular.txt fwl/adjectives/28K_adjectives.txt | tr A-Z a-z > adj.txt
python2 popularwords/filter.py popularwords/popular.txt fwl/adverbs/6K_adverbs.txt | tr A-Z a-z  > adv.txt
python2 popularwords/filter.py popularwords/popular.txt fwl/nouns/91K_nouns.txt > nou.txt
python2 popularwords/filter.py popularwords/popular.txt fwl/verbs/31K_verbs.txt | tr A-Z a-z  > verb.txt
python2 popularwords/filter.py popularwords/popular.txt fwl/arts.txt | tr A-Z a-z  > art.txt
